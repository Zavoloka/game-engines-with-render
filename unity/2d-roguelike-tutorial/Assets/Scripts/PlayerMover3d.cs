﻿using UnityEngine;
using System.Collections;

public class PlayerMover3d : MonoBehaviour {

    float _speed = 2;

    Vector3 _movement;
    Rigidbody _rb;

    Animator _anim;
    int _animatorIsWalkingHash = Animator.StringToHash("IsWalking");

    // Use this for initialization
    void Start () {

        _rb = GetComponent<Rigidbody>();
        _anim = GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal"); // TODO : repalace by code
        float v = Input.GetAxis("Vertical"); // TODO : same

        Debug.Log("h : " + h.ToString() + " / " + "v : " + v.ToString());

        Move(h, v);
        Animate(h, v);
        
	}
    void Move(float h, float v)
    {
        _movement.Set(-h, 0f, -v);
        _movement = this._movement.normalized * _speed * Time.deltaTime;

        // -------------------------------------------------------------
        //DebugText("Time DeltaTime : " + Time.deltaTime.ToString());
        //DebugText("Movement : " + this._movement.ToString());
        //DebugText("Transform Position : " + this.transform.position.ToString());


        _rb.MovePosition(transform.position + _movement);

    }

    void Animate(float h, float v)
    {

        bool walking = (h != 0f) || (v != 0f);
        //this._anim.SetBool("IsWalking", walking);
        _anim.SetBool(_animatorIsWalkingHash, walking);
    }
}
