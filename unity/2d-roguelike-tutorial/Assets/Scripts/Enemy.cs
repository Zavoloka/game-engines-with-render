﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : MovingObject {

    public int PlayerDamage;

    private Animator _animatorComponent;
    private Transform _target;
    private bool _skipMove;

	// Use this for initialization
	protected override void Start ()
    {
        GameManager.Instance.AddEnemyToList(this);
        _animatorComponent = GetComponent<Animator>();
        _target = GameObject.FindGameObjectWithTag("Player").transform;
        //GameObject player = GameObject.FindGameObjectWithTag("Player");
        //Debug.Assert(player != null);

        base.Start();
	
	}

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        if (_skipMove)
        {
            _skipMove = false;
            return;
        }

        base.AttemptMove<T>(xDir, yDir);
        _skipMove = true;

    }

    public void MoveEnemy()
    {
        int xDir = 0;
        int yDir = 0;

        if (Mathf.Abs(_target.position.x - this.transform.position.x) < float.Epsilon)
        {
            yDir = _target.position.y > this.transform.position.y ? 1 : -1;
        } else
        {
            xDir = _target.position.x > this.transform.position.x ? 1 : -1;
        }
        AttemptMove<Player>(xDir, yDir);

    }

    protected override void OnCantMove<T>(T component)
    {
        Player hitPlayer = component as Player;
        _animatorComponent.SetTrigger("enemyAttack");
        hitPlayer.LoseFood(PlayerDamage);

     
    }

}
