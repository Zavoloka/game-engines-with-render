﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    // TODO : refator tutorial code
    public static GameManager Instance = null;
    public int PlayerFoodPoints = 100;
    [HideInInspector]
    public bool PlayersTurn = true;
    public float TurnDelay = .1f;

    private BoardManager _boardScript;
    private int _level = 3;
    private List<Enemy> _enemies;
    private bool _enemiesMoving;


    public void GameOver()
    {

        enabled = false;
    }


    public void AddEnemyToList(Enemy script)
    {
        _enemies.Add(script);
        Debug.Log("Enemy was added");
        Debug.Log("Current enemy count : " + _enemies.Count);

    }



    void Awake()
    {
        // TODO : debug singleton
        if (Instance == null)
        {
            Instance = this;
        } else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        _enemies = new List<Enemy>();
        //Instance._enemies = new List<Enemy>();
        _boardScript = GetComponent<BoardManager>();
        InitGame();
        Debug.Log("GameManager Awake");

    }

    void Start()
    {
        Debug.Log("GameManager Start");
    }

    void InitGame()
    {
        // Instance._enemies.Clear();
        _enemies.Clear();
        Debug.Log("Current enemy count : " + _enemies.Count);
        _boardScript.SetupScene(_level);

    }

    private void Update()
    {

        if (PlayersTurn || _enemiesMoving)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
    }


    IEnumerator MoveEnemies()
    {
        _enemiesMoving = true;
        yield return new WaitForSeconds(TurnDelay);

        if (_enemies.Count == 0)
        {
            yield return new WaitForSeconds(TurnDelay);
        }

        for (int i = 0; i < _enemies.Count; i++)
        {

            Debug.Log(_enemies.Count.ToString() + " i:  " + i.ToString());
            // FIX IT for 2nd level
            _enemies[i].MoveEnemy();
            yield return new WaitForSeconds(_enemies[i].MoveTime);
      
        }

        PlayersTurn = true;
        _enemiesMoving = false;


    }

  
}
