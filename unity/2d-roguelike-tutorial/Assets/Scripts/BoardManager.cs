﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[Serializable]
public class Count
{
    public int Minumum;
    public int Maximum;

    public Count(int min, int max)
    {
        Minumum = min;
        Maximum = max;

    }
}

public class BoardManager : MonoBehaviour {

    // TODO : refactor this code from tut

    // TODO : replace int 
    public int Columns = 8;
    public int Rows = 8;

    public Count WallCount = new Count(5, 9);
    public Count FoodCount = new Count(1, 5);
    public GameObject PlayerTile;
    public GameObject ExitTile;
    public GameObject[] FloorTiles;
    public GameObject[] WallTiles;
    public GameObject[] FoodTiles;
    public GameObject[] EnemyTiles;
    public GameObject[] OuterWallTiles;


    private Transform _boardHolder;
    private List<Vector3> _gridPosition = new List<Vector3>();



    public void SetupScene(int level)
    {
        BoardSetup();

        InitializeList();

        LayoutObjectAtRandom(WallTiles, WallCount.Minumum, WallCount.Maximum);
        LayoutObjectAtRandom(FoodTiles, FoodCount.Minumum, FoodCount.Maximum);


        int enemyCount = (int)Mathf.Log(level, 2f);
        Debug.Log("Level : " + level.ToString());
        Debug.Log("Enemy count : " + enemyCount.ToString());

        LayoutObjectAtRandom(EnemyTiles, enemyCount, enemyCount, true);
        Instantiate(ExitTile, new Vector3(Columns - 1, Rows - 1, 0f), Quaternion.identity);
        Instantiate(PlayerTile, new Vector3(0f, 0f, 0f), Quaternion.identity);

    }

    // ---------------------------------------------

    void InitializeList()
    {

        _gridPosition.Clear();
        for (int x = 1; x < Columns - 1; x++)
        {
            for (int y = 1; y < Rows - 1; y++)
            {

                _gridPosition.Add(new Vector3(x, y, 0f));
            }
        }
    }

    void BoardSetup()
    {

        _boardHolder = new GameObject("Board").transform;

        for (int x = -1; x < Columns + 1; x++)
        {
            for (int y = -1; y < Rows + 1; y++)
            {

                GameObject toInstantiate = FloorTiles[Random.Range(0, FloorTiles.Length)];
                if (x == -1 || x == Columns || y == -1 || y == Rows)
                {
                    toInstantiate = OuterWallTiles[Random.Range(0, OuterWallTiles.Length)];
                }
                    
                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(_boardHolder);
            }
        }

    }

    Vector3 RandomPosition()
    {

        int randomIndex = Random.Range(0, _gridPosition.Count);
        Vector3 randomPosition = _gridPosition[randomIndex];
        _gridPosition.RemoveAt(randomIndex);

        return randomPosition;
    }


    void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum, bool debug = false)
    {
        int objectCount = Random.Range(minimum, maximum + 1);
        if (debug)
        {
            Debug.Log("Layout Object Enemy (count) : " + objectCount.ToString());
        }


        for (int i = 0; i < objectCount; i++)
        {
            Vector3 randomPosition = RandomPosition();
            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }

    }


}
