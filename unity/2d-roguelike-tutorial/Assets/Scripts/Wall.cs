﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public AudioClip ChopSound1;
    public AudioClip ChopSound2;
    public Sprite DmgSprite;
    public int Hp = 4;

    private SpriteRenderer _spriteRendererComponent;

    private void Awake()
    {
        _spriteRendererComponent = GetComponent<SpriteRenderer>();
    }

    public void DamageWall(int loss)
    {

        // SoundManager.instance.RandomizeSfx(ChopSound1, ChopSound2);

        _spriteRendererComponent.sprite = DmgSprite;
        Hp -= loss;
        if (Hp <= 0)
        {
            gameObject.SetActive(false);
        }


    }

}