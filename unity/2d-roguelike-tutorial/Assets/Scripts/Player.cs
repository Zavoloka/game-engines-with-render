﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class Player : MovingObject {


    public int WallDamage = 1;
    public int PointsPerFood = 10;
    public int PointsPerSoda = 20;
    public float RestartLevelDelay = 1f;

    private Animator _animatorComponent;
    private int _food;

    // Use this for initialization
    protected override void Start()
    {
        _animatorComponent = GetComponent<Animator>();
        _food = GameManager.Instance.PlayerFoodPoints;

        base.Start();

    }

    public void OnDisable()
    {
        GameManager.Instance.PlayerFoodPoints = _food;
    }

    // Update is called once per frame
    void Update()
    {

        if (!GameManager.Instance.PlayersTurn)
        {
            return;
        }

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
        {
            vertical = 0;
        }

        if (horizontal != 0 || vertical != 0)
            AttemptMove<Wall>(horizontal, vertical);


    }

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        _food--;
        base.AttemptMove<T>(xDir, yDir);

        RaycastHit2D hit;

        if (Move(xDir, yDir, out hit))
        {
            // ------------
        }


        CheckIfGameOver();

        GameManager.Instance.PlayersTurn = false;

    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        switch (other.tag)
        {
            case "Exit":
                Invoke("Restart", RestartLevelDelay);
                enabled = false;
                break;

            case "Food":
                _food += PointsPerFood;
                other.gameObject.SetActive(false);
                break;

            case "Soda":
                _food += PointsPerSoda;
                other.gameObject.SetActive(false);
                break;
            default:
                break;
        }

    

    }

    protected override void OnCantMove<T>(T component)
    {
        Wall hitWall = component as Wall;
        hitWall.DamageWall(WallDamage);
        _animatorComponent.SetTrigger("playerChop");

    }

    private void Restart()
    {

        SceneManager.LoadScene(0);

    }

    public void LoseFood(int loss)
    {
        _animatorComponent.SetTrigger("playerHit");
        _food = loss;
        CheckIfGameOver();

    }

    private void CheckIfGameOver()
    {

        if (_food <= 0)
        {

            GameManager.Instance.GameOver();
        }
    }

}
