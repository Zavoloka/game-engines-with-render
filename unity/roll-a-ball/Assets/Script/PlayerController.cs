﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{


    private Rigidbody _rb;
    private ushort _count;

    public float Speed;
    public Text WinText;
    public Text CountText;

    // first frame of the game (init)
    void Start()
    {
        // init our rigit body 
        _rb = GetComponent<Rigidbody>();
        this._count = 0;
        SetCountText();
        this.WinText.text = "";


    }


    // before frame rendering
    void Update()
    {


    }


    // before physics caluclation
    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        this._rb.AddForce(movement * this.Speed);
    }

    // triger on collide
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick_Up"))
        {
            other.gameObject.SetActive(false);
            this._count += 1;
            SetCountText();
        }

    }

    void SetCountText()
    {
        ushort maxCountValue = 12;
        this.CountText.text = "Cats total : " + this._count + "/" + maxCountValue;
        if (this._count >= maxCountValue)
        {
            this.WinText.text = "You Win!";
        }

    }
}

