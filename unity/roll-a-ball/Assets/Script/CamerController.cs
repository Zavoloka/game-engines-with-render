﻿using UnityEngine;
using System.Collections;

public class CamerController : MonoBehaviour {

    public GameObject Player;

    private Vector3 _offset = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {

        // calcalate offset between camera position and player position (delta)
        this._offset = transform.position - this.Player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {

        // move camera according to the player position
        transform.position = this.Player.transform.position + this._offset;
	
	}


}
