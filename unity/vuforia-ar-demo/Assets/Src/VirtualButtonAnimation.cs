﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

namespace Assets.Src
{
    public class VirtualButtonAnimation : MonoBehaviour, IVirtualButtonEventHandler
    {

        public GameObject VirtualButtonObject;
        public float DaySpeed = 100.0f;
        public IDayCycle DayCycle;

        // Use this for initialization
        void Start()
        {

            VirtualButtonObject = GameObject.Find("VirtualButton");
            VirtualButtonObject.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

            DayCycle = GameObject.Find("DirectionalLight").GetComponent<IDayCycle>();

        }


        public void OnButtonPressed(VirtualButtonBehaviour vbb)
        {
            //DayCycle.ProcessDayCycle(DaySpeed);
            //Debug.Log("Button pressed");
        }


        public void OnButtonReleased(VirtualButtonBehaviour vbb)
        {

            // do smth
            ///Debug.Log("Button released");

        }


        // Update is called once per frame
        void Update()
        {

        }
    }

}