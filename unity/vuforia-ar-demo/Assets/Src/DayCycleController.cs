﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Src
{

    public class DayCycleController : MonoBehaviour, IDayCycle
    {

        public float daySpeed = 5.0f;

        // Use this for initialization
        void Start()
        {

        }

        public void ProcessDayCycle(float daySpeed)
        {
            this.transform.Rotate(daySpeed * Time.deltaTime, 0.0f, 0.0f);

        }

        // Update is called once per frame
        void Update()
        {

            ProcessDayCycle(daySpeed);

        }
    }

}
