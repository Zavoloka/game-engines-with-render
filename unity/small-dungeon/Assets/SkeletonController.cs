﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonController : MonoBehaviour {

    //private _child;
    bool canJump = true;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

  
        float speed = 1f;
        float rotationSpeed = 5f;
        float jumpHeight = 5f;
  

        // get key down

        float translationVertical = Input.GetAxis("Vertical") * speed;
        float translationHorizontal = Input.GetAxis("Horizontal") * speed;
        bool isRotationLeft = Input.GetKey(KeyCode.E);
        bool isRotationRight = Input.GetKey(KeyCode.Q);
        bool jump = Input.GetButton("Jump");
     

        //Debug.Log("jump check : " + jump.ToString());

        float rotationLeft = isRotationLeft ? +1 : 0;
        float rotationRight = isRotationRight ? -1 : 0;
        if (jump) {

            performJump(jumpHeight);
        }
     
       
        Debug.Log("CanJump : " + canJump.ToString());

        Vector3 rotation = new Vector3(0, (rotationLeft + rotationRight) * rotationSpeed, 0);

        translationVertical *= Time.deltaTime;
        translationHorizontal *= Time.deltaTime;
  


        //transform.position = transform.position + new Vector3(translationHorizontal, JumpValue, translationVertical);
        //transform.Translate(translationHorizontal, 0, translationVertical);
        Vector3 newPosition = new Vector3(translationHorizontal, 0, translationVertical);
        transform.position = transform.position + newPosition;

        //Debug.Log("Position : " + newPosition.ToString());
        //GetComponent<Rigidbody>().AddForce(newPosition * 200, ForceMode.Force);
        //GetComponent<Rigidbody>().MovePosition(newPosition);

        transform.Rotate(rotation);
      


        /*
         * 
         *      void jump(){
         if (canJump) {
             canJump = false;
             GetComponent<Rigidbody>().AddForce (this.gameObject.transform.up * jumpForce);
         }
     }
 
     void OnCollisionEnter (Collision collidingObject) {
         if (collidingObject.gameObject.layer == 8) {
             canJump = true;
         }
     }*/
    }
    void performJump(float jumpHeightInner)
    {
        if (true)
        {
            canJump = false;
            //StartCoroutine(jumpAnimation(jumpHeightInner));
            Vector3 newPosition = new Vector3(0, jumpHeightInner * Time.deltaTime, 0);
            transform.position = transform.position + newPosition;
        }
    }



        // perhpase start coroutine

     void OnCollisionEnter(Collision collidingObject)
    {
        // 8 = floor
        if (collidingObject.gameObject.layer == 8)
        {
            canJump = true;
        }
    }
}
