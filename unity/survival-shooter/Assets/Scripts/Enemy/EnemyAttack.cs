﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public float TimeBetweenAttacks = 0.5f;
    public int AttackDamage = 10;


    private Animator _anim;
    private GameObject _player;
    private PlayerHealth _playerHealthScript;
    EnemyHealth _enemyHealth;
    private bool _playerInRange;
    private float _timer;


    void Awake ()
    {
        _player = GameObject.FindGameObjectWithTag ("Player");
        _playerHealthScript = _player.GetComponent <PlayerHealth> ();
        _enemyHealth = GetComponent<EnemyHealth>();
        _anim = GetComponent <Animator> ();
    }


    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject == _player)
        {
            _playerInRange = true;
        }
    }


    void OnTriggerExit (Collider other)
    {
        if(other.gameObject == _player)
        {
            _playerInRange = false;
        }
    }


    void Update ()
    {
        _timer += Time.deltaTime;

        if(_timer >= TimeBetweenAttacks && _playerInRange && _enemyHealth.CurrentHealth > 0)
        {
            Attack ();
        }

        if(_playerHealthScript.CurrentHealth <= 0)
        {
            _anim.SetTrigger ("PlayerDead");
        }
    }


    void Attack ()
    {
        _timer = 0f;

        if(_playerHealthScript.CurrentHealth > 0)
        {
            _playerHealthScript.TakeDamage (AttackDamage);
        }
    }
}
