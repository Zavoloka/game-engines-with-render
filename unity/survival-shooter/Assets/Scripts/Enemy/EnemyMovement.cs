﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform _playerTransform;
    PlayerHealth _playerHealth;
    EnemyHealth _enemyHealth;
    NavMeshAgent _nav;


    void Awake ()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        _playerHealth = _playerTransform.GetComponent <PlayerHealth> ();
        _enemyHealth = GetComponent <EnemyHealth> ();
        _nav = GetComponent<NavMeshAgent>();
    }


    void Update ()
    {

        bool navEnabled = false;
        if(_enemyHealth.CurrentHealth > 0 && _playerHealth.CurrentHealth > 0)
        {
           _nav.SetDestination(_playerTransform.position);
           navEnabled = true;
        }
        _nav.enabled = true;
      
    }
}
