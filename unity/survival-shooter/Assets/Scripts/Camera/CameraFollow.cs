﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public Transform Target;
    public float Smoothing = 5f;

    // Dark Lighting map help : 
    // https://answers.unity.com/questions/919940/applicationloadlevel-changes-lighting-for-some-rea.html
    // https://answers.unity.com/questions/925671/very-weird-dark-level-loading.html

    private Vector3 _cameraOffset;

    void Start () {

        this._cameraOffset = this.transform.position - this.Target.position;
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 targetCamPos = this.Target.position + this._cameraOffset;

        this.transform.position = Vector3.Lerp(this.transform.position, targetCamPos,
                                                this.Smoothing * Time.deltaTime);
	
	}
}
