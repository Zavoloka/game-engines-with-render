﻿using System;
using System.Globalization;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public bool DebugParam = false;
    public float Speed = 6f;
    public LayerMask FloorMask;

    private Vector3 _movement;
    private Animator _anim;
    private Rigidbody _rb;
    private int _floorMask;
    private float _camRayLength = 500f;
    private int _animatorIsWalkingHash = Animator.StringToHash("IsWalking");

    // TODO : remove unnecessary 'this'


    private GameObject _debugCube;

    private void Awake()
    {

        this._floorMask = LayerMask.GetMask("Floor");

        //DebugText("Floor mask " + this._floorMask);

        this._anim = GetComponent<Animator>();
        this._rb = GetComponent<Rigidbody>();

        // ----------------------------

        this._debugCube = GameObject.FindGameObjectWithTag("DebugCube");

    }

    private void FixedUpdate()
    {

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);

        Turning();

        Animating(h, v);


    }

    void Move(float h, float v)
    {

        this._movement.Set(h, 0f, v);

        this._movement = this._movement.normalized * this.Speed * Time.deltaTime;

        // -------------------------------------------------------------
        //DebugText("Time DeltaTime : " + Time.deltaTime.ToString());
        //DebugText("Movement : " + this._movement.ToString());
        //DebugText("Transform Position : " + this.transform.position.ToString());


        this._rb.MovePosition(this.transform.position + _movement);


    }

    void Turning()
    {

        // TODO : add debug code
        // TODO : add comments

        // Issue resolve
        // https://answers.unity.com/questions/573213/raycast-not-detecting-planes.html

        //Camera camera = GameObject.FindGameObjectWithTag("MainCamera");
        //ray from mouse cursor on screen in the direction to camera

        Vector3 currentMousePosition = Input.mousePosition;
        Ray camRay = Camera.main.ScreenPointToRay(currentMousePosition);

        //DebugText("Current Mouse Position : " + currentMousePosition.ToString());
        //DebugText("Cam Ray : " + camRay.origin.ToString() + " / " + camRay.direction.ToString());
        //Debug.DrawRay(camRay.origin, camRay.direction, Color.red);

        RaycastHit floorHit;
        bool floorHitCheck = Physics.Raycast(camRay, out floorHit, this._camRayLength, this.FloorMask);

        //Ray testRay = new Ray(new Vector3(0, 20, 0), new Vector3(0, -20, 0));
        //floorHitCheck = Physics.Raycast(testRay, out floorHit, this._camRayLength, this.FloorMask);
        //DebugText("Floor hit check : " + floorHitCheck.ToString());
        //Debug.DrawLine(camRay.origin, camRay.direction, Color.cyan);


        // return;
        //Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(currentMousePosition);
        //Ray camRay = new Ray(Camera.main.transform.position, mouseWorldPosition);

        //DebugText("Mouse Position : " + currentMousePosition.ToString());
        //DebugText("Mouse World Position : " + currentMousePosition.ToString());
        //DebugText("Camera Position  : " + Camera.main.transform.position.ToString());
        //DebugText("Cam Ray : " + camRay.ToString());

        //RaycastHit floorHit;

        //DebugText("Transform Position : " + this.transform.position.ToString());
        //bool floorHitCheck = Physics.Raycast(camRay, out floorHit, this._camRayLength, this.FloorMask);
 
        //Vector3 vectorFromRay = camRay.direction - camRay.origin;

        // --------------------------------------------------------------------------
        //DebugText("Transform Position : " + this.transform.position.ToString());
        //DebugText("Floor hit check : " + floorHitCheck.ToString());
        //DebugText("Floor hit out var : " + floorHit.point.ToString());
        //DebugText("Ray length : " + vectorFromRay.magnitude.ToString());
        //DebugText("Cam Ray : " + camRay.origin.ToString() + " / " + camRay.direction.ToString());

        //Debug.DrawLine(camRay.origin, camRay.direction, Color.blue);

        // --------------------------------------------------------------------------

        //Ray rayFromBox = new Ray(this._debugCube.transform.position, currentMousePosition + Vector);
        //RaycastHit floorHitFromCube;

        //bool floorHitCubeCheck = Physics.Raycast(rayFromBox, out floorHitFromCube, this._camRayLength, this.FloorMask);

        //DebugText("Floor hit check (Cube) : " + floorHitCubeCheck.ToString());
        //DebugText("Mouse Position (Cube) : " + currentMousePosition.ToString());
        //DebugText("Cam Ray (Cube) : " + rayFromBox.origin.ToString() + " / " + rayFromBox.direction.ToString());
        //Debug.DrawLine(rayFromBox.origin, rayFromBox.direction, Color.red);


        if (floorHitCheck)
        {

            Vector3 playerToMouse = floorHit.point - transform.position;
     
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            this._rb.MoveRotation(newRotation);


        }
    }


    void Animating(float h, float v)
    {

        bool walking = (h != 0f) || (v != 0f);
        //this._anim.SetBool("IsWalking", walking);
        this._anim.SetBool(this._animatorIsWalkingHash, walking);
    }


    void DebugText(string text)
    {

        DateTime localDate = DateTime.Now;
        if (this.DebugParam)
        {
            //Debug.Log(text + " (" + localDate.ToString() + ")");
            Debug.Log(text);
        }
    }



}
