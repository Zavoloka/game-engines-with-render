﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public int StartingHealth = 100;
    public int CurrentHealth;
    public Slider HealthSlider;
    public Image DamageImage;
    public AudioClip DeathClip;
    public float FlashSpeed = 5f;
    public Color FlashColour = new Color(1f, 0f, 0f, 0.1f);


    private Animator _anim;
    private AudioSource _playerAudio;
    private PlayerMovement _playerMovementScript;
    private PlayerShooting _playerShooting;
    private bool _isDead;
    private bool _damaged;

    private int _animatorDieHash = Animator.StringToHash("Die");


    void Awake ()
    {
        _anim = GetComponent <Animator> ();
        _playerAudio = GetComponent <AudioSource> ();
        _playerMovementScript = GetComponent <PlayerMovement> ();
        _playerShooting = GetComponentInChildren <PlayerShooting> ();
        CurrentHealth = StartingHealth;
    }


    void Update ()
    {
        if(_damaged)
        {
            DamageImage.color = FlashColour;
        }
        else
        {
            DamageImage.color = Color.Lerp (DamageImage.color, Color.clear, FlashSpeed * Time.deltaTime);
        }
        _damaged = false;
    }


    public void TakeDamage (int amount)
    {
        _damaged = true;

        CurrentHealth -= amount;

        HealthSlider.value = CurrentHealth;

        _playerAudio.Play ();

        if(CurrentHealth <= 0 && !_isDead)
        {
            Death ();
        }
    }


    void Death ()
    {
        _isDead = true;

        _playerShooting.DisableEffects ();

        _anim.SetTrigger (_animatorDieHash);

        _playerAudio.clip = DeathClip;
        _playerAudio.Play ();

        _playerMovementScript.enabled = false;
        _playerShooting.enabled = false;
    }


    public void RestartLevel ()
    {
        SceneManager.LoadScene (0);
    }
}
