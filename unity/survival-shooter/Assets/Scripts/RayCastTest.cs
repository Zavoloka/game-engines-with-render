﻿using UnityEngine;
using System.Collections;

public class RayCastTest : MonoBehaviour {

    // Unity3D Scripting - Raycasting 
    // https://youtu.be/1z7y72DWlNU

    public float MaxRayDistance = 17;
    public LayerMask ActiveLayer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void FixedUpdate()
    {
        Ray ray = new Ray(this.transform.position, Vector3.right);
        // (*)
        //RaycastHit hit;
        //bool raycastHitBool = Physics.Raycast(ray, out hit, this.MaxRayDistance);

        RaycastHit[] hits = Physics.RaycastAll(ray, this.MaxRayDistance, this.ActiveLayer);

        Debug.DrawLine(this.transform.position, 
                       this.transform.position + Vector3.right * this.MaxRayDistance,
                       Color.red);

        /* (*)
        if (raycastHitBool)
        {
            Debug.DrawLine(hit.point, hit.point + Vector3.up * 2, Color.green);
            Debug.Log("We have a hit !");
        }
        */


        foreach (RaycastHit hit in hits)
        {
            Debug.DrawLine(hit.point, hit.point + Vector3.up * 2, Color.green);
        }

    }
}
