﻿using UnityEngine;
using System.Collections;

public class BGScroller : MonoBehaviour {

    public float ScrollSpeed;

    private float _tileSeizeZ;
    private Vector3 _startPosition;

	// Use this for initialization
	void Start () {
        this._startPosition = transform.position;
        this._tileSeizeZ = transform.localScale.y;
	}
	
	// Update is called once per frame
	void Update () {

        float newPosition = Mathf.Repeat(Time.time * this.ScrollSpeed, this._tileSeizeZ);
        transform.position = this._startPosition + Vector3.forward * newPosition;
	}
}
