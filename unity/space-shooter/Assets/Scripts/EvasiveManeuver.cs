﻿using UnityEngine;
using System.Collections;

public class EvasiveManeuver : MonoBehaviour
{

    public float Dodge;
    public float Smoothing;
    public float Tilt;

    public Vector2 StartWait;
    public Vector2 ManeuverTime;
    public Vector2 ManeuverWait;
    public Boundary Boundary;

    private float _currentSpeed;
    private float _targetManeuver;
    private Rigidbody _rb;

    void Start()
    {
        this._rb = GetComponent<Rigidbody>();
        this._currentSpeed = this._rb.velocity.z;
        StartCoroutine (Evade ());

    }

    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(this.StartWait.x, this.StartWait.y));
        while (true)
        {
            // from enemy prespective ?
            this._targetManeuver = Random.Range(1, this.Dodge) * -Mathf.Sign(this.transform.position.x);
            yield return new WaitForSeconds(Random.Range(this.ManeuverTime.x, this.ManeuverTime.y));
            this._targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(this.ManeuverWait.x, this.ManeuverWait.y));

        }

    }

    private void FixedUpdate()
    {
        float newManeuver = Mathf.MoveTowards(this._rb.velocity.x, this._targetManeuver, Time.deltaTime * this.Smoothing);
        this._rb.velocity = new Vector3(newManeuver, 0.0f, this._currentSpeed);
        this._rb.position = new Vector3
            (
                Mathf.Clamp(this._rb.position.x, this.Boundary.Xmin, this.Boundary.Xmax),
                0.0f,
                Mathf.Clamp(this._rb.position.z, this.Boundary.Zmin, this.Boundary.Zmax)
            );
        _rb.rotation = Quaternion.Euler(0.0f, 0.0f, this._rb.velocity.x * -this.Tilt);

    }

    //     Debug.Log("Mouse position: " + moveDirection + "   Char position: " + playerPosition);




}
