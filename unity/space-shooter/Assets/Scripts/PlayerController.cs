﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{

    public float Xmin, Xmax, Zmin, Zmax; 
}

public class PlayerController : MonoBehaviour {

    public float Speed;
    public float Tilt;
    public Boundary Boundaries;

    public GameObject Shot;
    public Transform ShotSpawn;
    public float FireRate = 0.4f;


    private Rigidbody _rb;
    private float _nextFire;
    private AudioSource _boltAudio;


    /* NOT ALLOWED
     *
     * GetComponentFastPath is not allowed to be called from a MonoBehaviour constructor
     *  (or instance field initializer), call it in Awake or Start instead. 
     *  Called from MonoBehaviour 'PlayerController' on game object 'Player'.
     *  See "Script Serialization" page in the Unity Manual for further details.
     *  UnityEngine.Component:GetComponent()
     *  PlayerController:.ctor() (at Assets/Scripts/PlayerController.cs:12)
     *
     * 
    /* 
    PlayerController() : base()
    {
        _rb = GetComponent<Rigidbody>();
    }
    */

    // Use this for initialization
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _boltAudio = GetComponent<AudioSource>();
    }

    void Update()
    {

        if (Input.GetButton("Fire1") && Time.time > this._nextFire)
        {
            this._nextFire = Time.time + this.FireRate;
            Instantiate(Shot, this.ShotSpawn.position, this.ShotSpawn.rotation);
            //_boltAudio.Play(); // audio bolt was implemented by 'Bolt' prefab side
        }
    }

    private void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        _rb.velocity = movement * Speed;
        Vector3 position = new Vector3(
                            Mathf.Clamp(_rb.position.x, Boundaries.Xmin, Boundaries.Xmax), 
                            _rb.position.y, 
                            Mathf.Clamp(_rb.position.z, Boundaries.Zmin, Boundaries.Zmax)
                        );

        _rb.position = new Vector3(
                            Mathf.Clamp(_rb.position.x, Boundaries.Xmin, Boundaries.Xmax),
                            _rb.position.y,
                            Mathf.Clamp(_rb.position.z, Boundaries.Zmin, Boundaries.Zmax)
                        );
        _rb.rotation = Quaternion.Euler(0.0f, 0.0f, _rb.velocity.x * -this.Tilt);



    }


}
