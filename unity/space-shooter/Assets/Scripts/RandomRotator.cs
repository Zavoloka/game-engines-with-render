﻿using UnityEngine;
using System.Collections;

public class RandomRotator : MonoBehaviour {

    public float Tumble;
    private Rigidbody _rb;

	// Use this for initialization
	void Start () {

        this._rb = GetComponent<Rigidbody>();
        this._rb.angularVelocity = Random.insideUnitSphere * this.Tumble;
	
	}
	
}
