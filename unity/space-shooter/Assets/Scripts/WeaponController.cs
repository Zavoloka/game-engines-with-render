﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour {

    public GameObject Shot;
    public Transform ShotSpawn;
    public Vector2 FireRate;
    public float Delay; 

	// Use this for initialization
	void Start () {

        InvokeRepeating("Fire", this.Delay, Random.Range(this.FireRate.x, this.FireRate.y));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Fire()
    {

        Instantiate(this.Shot, this.ShotSpawn.position, this.ShotSpawn.rotation);
    }
}
