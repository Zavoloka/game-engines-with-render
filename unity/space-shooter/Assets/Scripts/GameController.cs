﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

    public GameObject[] Hazards; 
    public Vector3 SpawnValues;

    public int HazardCount;
    public float SpawnWait;
    public float StartWait;
    public float WaveWait;

    public Text ScoreText;
    public Text RestartText;
    public Text GameOverText;

    private int _hazardsLength = 0;
    private bool _gameOver;
    private bool _restart;
    private int _score;


	// Use this for initialization
	void Start () {

        this._gameOver = false;
        this._restart = false;
        this.GameOverText.text = "";
        this.RestartText.text = "";
        this._hazardsLength = this.Hazards.Length;

        this._score = 0;
        UpdateScore();
        StartCoroutine(SpawnWaves());
	}

    private void Update()
    {
        if (this._restart)
        {

            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
    }

    IEnumerator SpawnWaves()
    {

        yield return new WaitForSeconds(this.StartWait);
        while (true)
        {
            for (int i = 0; i < this.HazardCount; i++)
            {
                GameObject hazard = this.Hazards[Random.Range(0, this._hazardsLength)];
                Vector3 spawnPosition = new Vector3(
                            Random.Range(-this.SpawnValues.x, this.SpawnValues.x),
                            this.SpawnValues.y,
                            SpawnValues.z
                        );

                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(this.SpawnWait);
            }

            yield return new WaitForSeconds(this.WaveWait);

            if (this._gameOver)
            {
                this.RestartText.text = "Press 'R' for Restart";
                this._restart = true;
                break;

            }

        }

    }

    public void AddScore(int scoreIncrementer)
    {
        this._score += scoreIncrementer;
        UpdateScore();

    }

    void UpdateScore()
    {
        this.ScoreText.text = "Score: " + this._score;
    }

    public void GameOver()
    {

        this.GameOverText.text = "Game Over";
        this._gameOver = true;
    }


}

