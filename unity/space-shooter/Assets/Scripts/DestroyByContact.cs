﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

    public GameObject Explosion;
    public GameObject PlayerExplosion;

    public int ScoreValue;
    private GameController _gameController;

    private void Start()
    {

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            this._gameController = gameControllerObject.GetComponent<GameController>();
        }

        if (this._gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }

    }

    private void OnTriggerEnter(Collider other)
    {

        //Debug.Log(other.name);
        // if (other.tag == "Boundary")
        if (other.CompareTag("Boundary") || other.CompareTag("Enemy"))    
        {
            return;
        }

        if (this.Explosion != null)
        {
            Instantiate(this.Explosion, transform.position, transform.rotation);
        }


        //if (other.tag == "Player")
        if (other.CompareTag("Player"))
        {
            Instantiate(this.PlayerExplosion, other.transform.position, other.transform.rotation);
            _gameController.GameOver();
        }

        this._gameController.AddScore(this.ScoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
